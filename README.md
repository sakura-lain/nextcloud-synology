# Nextcloud on Synology

Installation, configuration and upgrade steps for Nextcloud on a Synology with DSM 6.

*Main source :* https://docs.nextcloud.com/server/latest/admin_manual/maintenance/manual_upgrade.html
*Téléchargement des sous-versions les plus récentes de chaque version majeure* : https://download.nextcloud.com/server/releases/

## Config générale

- Un dossier partagé est créé pour les données de Nextcloud : `nextcloud_data` appartenant à l'utilisateur et au groupe http avec les permissions 770.
- Le dossier contenant les fichiers de configuration de Nextcloud se situe dans `/volume1/web/nextcloud`
- Un sous-sous-domaine est attribué à Nextcloud : `nextcloud.mysubdomain.synology.me`

## Upgrade de 17 vers 18

### Installation des fichiers de la version 18

- Optionnel : arrêt de WebStation :
```
synoservice --status pkgctl-WebStation
synoservice --stop pkgctl-WebStation
```
(attention freezes potentiels)
- Sauvegarder l'installation actuelle avec `rsync -av`
- Dézipper la version 18 à l'emplacement du site web (/volume1/web)
- Copier le fichier `config.php` de la version précédemment installée
- Rétablir les permissions :
```
chown -R http:http nextcloud
```
Optionnel (attention peut freezer) :
```
find nextcloud/ -type d -exec chmod 750 {} \;
find nextcloud/ -type f -exec chmod 640 {} \;
```

### Modification de la version PHP utilisée par DSM

L'upgrade se fait en se basant sur la version de PHP utilisée de manière globale par DSM. Or celle-ci est trop ancienne pour la version 18 de Nextcloud.
- Changer la version de PHP par défaut du Syno (5.6) vers 7.3 (7.2 minimum) :
```
cd /bin
sudo cp php php.bak
sudo rm php
sudo cp /volume1/@appstore/PHP7.2/usr/local/bin/php72 /bin/php
php -v
```
*Source :* https://community.synology.com/enu/forum/1/post/128808

### Mise à jour

- Updater avec la commande occ :
```
sudo -u http php occ upgrade
```
- Sortie du mode maintenance via le fichier `config.php`
#### Troubleshooting
- En cas d'erreur `Base table or view not found: 1146 Table ‘nextcloud.oc_flow_operations_scope’ doesn’t exist`
```
rm -f apps/workflowengine/appinfo/database.xml
sudo -u http php /volume1/web/nextcloud/occ upgrade
```
*Source :* https://help.nextcloud.com/t/nextcloud-oc-flow-operations-scope-doesnt-exist-after-nextcloud-upgrade-17-18/74873

### Pages blanches après mise à jour
- Si les pages "applications" et "utilisateurs" apparaissent blanches, supprimer le dossier `settings/` se situant dans la hiérarchie de Nextcloud (c'est un relicat de la version 17).

## Upgrade de 18 à 19

- Update vers 19 : refaire les étapes.
- Si pb version : la mettre à jour dans `config.php` vers 19.0.X.X.

## Upgrade de 19 à 20

- Au cas où la commande d'installation renvoie l'erreur suivante, la relancer pour terminer la configuration :

```
An unhandled exception has been thrown:
Error: Class 'OCA\Mail\AppInfo\BootstrapSingleton' not found in /volume1/web/nextcloud/apps/mail/lib/AppInfo/Application.php:35
```
- Redémarrer le serveur après l'upgrade.


## Configuration PHP 7.3

- Localisation `php.ini` (augmentation de la limite de mémoire) : https://www.tech2tech.fr/nas-synology-et-modification-php/
- Profil PHP 7.3 dans Webstation, modules activés :
  - bcmath
  - bz2
  - calendar
  - curl
  - exif
  - gd
  - gmp
  - iconv
  - intl
  - mysqli
  - openssl
  - pdo_mysql
  - posix
  - zip

Au cas où le message suivant s'affiche :

```
PHP Warning:  PHP Startup: Unable to load dynamic library 'mcrypt.so' (tried: /usr/local/lib/php73/modules/mcrypt.so (/usr/local/lib/php73/modules/mcrypt.so: cannot open shared object file: No such file or directory), /usr/local/lib/php73/modules/mcrypt.so.so (/usr/local/lib/php73/modules/mcrypt.so.so: cannot open shared object file: No such file or directory)) in Unknown on line 0
PHP Warning:  PHP Startup: Unable to load dynamic library 'mysql.so' (tried: /usr/local/lib/php73/modules/mysql.so (/usr/local/lib/php73/modules/mysql.so: cannot open shared object file: No such file or directory), /usr/local/lib/php73/modules/mysql.so.so (/usr/local/lib/php73/modules/mysql.so.so: cannot open shared object file: No such file or directory)) in Unknown on line
```

- Sauvegarder et éditer le fichier `/usr/local/etc/php73/cli/conf.d/phpMyAdmin.in` :

```
cp /usr/local/etc/php73/cli/conf.d/phpMyAdmin.ini /usr/local/etc/php73/cli/conf.d/phpMyAdmin.ini.bak
vim /usr/local/etc/php73/cli/conf.d/phpMyAdmin.ini
```

- Commenter les lignes suivantes :

```
extension=mcrypt.so
extension=mysql.so
```

## Configuration VirtualHost

- HSTS actif
- HTTP2 actif
- PHP 7.3
- Certificat dédié au sous-domaine

## Fichier config.php

A adapter selon les besoins :


```
<?php
$CONFIG = array (
  'instanceid' => 'myID',
  'passwordsalt' => 'mysalt',
  'secret' => 'mysecret',
  'trusted_domains' =>
  array (
    0 => '192.168.1.XX',
    1 => 'nextcloud.mysubdomain.synology.me',
  ),
  'datadirectory' => '/volume1/nextcloud_data',
  'dbtype' => 'mysql',
  'version' => '19.0.2.2',
  'overwrite.cli.url' => 'https://192.168.1.XX/nextcloud',
  'dbname' => 'myDBname',
  'dbhost' => '127.0.0.1:3307',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'dbuser' => 'myDBusername',
  'dbpassword' => 'myDBpassword',
  'installed' => true,
  'maintenance' => false,
  'has_rebuilt_cache' => true,
  'updater.secret' => '$myupdatersecret',
  'theme' => '',
  'loglevel' => 0,
);
```
