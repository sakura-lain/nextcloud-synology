#!/bin/bash
     
chown -R http:http /volume1/web/nextcloud/
chown -R http:http /volume1/web/nextcloud/apps/
chown -R http:http /volume1/web/nextcloud/config/
chown -R http:http /volume1/web/nextcloud/themes/
     
chown -R http:http /volume1/nextcloud_data/
     
chown http:http /volume1/web/nextcloud/.htaccess
     
find /volume1/web/nextcloud/ -type f -print0 | xargs -0 chmod 777
     
find /volume1/web/nextcloud/ -type d -print0 | xargs -0 chmod 777
find /volume1/nextcloud_data/ -type d -print0 | xargs -0 chmod 777
     
chmod 777 /volume1/web/nextcloud/.htaccess
